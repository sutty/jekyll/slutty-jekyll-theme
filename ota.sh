#!/bin/sh
set -e

function h1() {
  echo -e "\n\n# ${@}\n" >&2
}

function h2() {
  echo -e "\n\n## ${@}\n" >&2
}

function pre() {
  echo '```' >&2
}

# El primer parámetro tiene que ser el sitio
test -n "$1"

h1 "Entrar al repositorio ${1}"
cd "/srv/_sites/${1}"

h2 "Cambiando a la rama ${2}"

pre
  su -c "git fetch" rails
  su -c "git checkout ${2}" rails
pre

h2 "Traer cambios, moviendo los cambios locales arriba de todo en la historia (rebase)"

pre
  su -c 'git pull --rebase' rails
  su -c 'git lfs prune' rails
pre

h2 "Subir archivos binarios"

pre
  su -c 'git add public && git commit --message=public' rails || true
pre

h2 "Enviar cambios locales"

pre
  su -c 'git push' rails
pre

if test -n "${3}" ; then
  h2 "Publicando cambios en $1"

  pre
    cd /srv
    su -c "bundle exec rails r 'site = Site.find_by_name \"${1}\"; DeployJob.perform_now(site.id, notify: false, output: true)'" rails
  pre
fi

h2 "Listo"
