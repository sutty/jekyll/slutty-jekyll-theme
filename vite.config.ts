import { defineConfig, splitVendorChunkPlugin } from 'vite'
import RubyPlugin from 'vite-plugin-ruby'
import StimulusHMR from 'vite-plugin-stimulus-hmr'

export default defineConfig({
  plugins: [
    RubyPlugin(),
    StimulusHMR(),
    splitVendorChunkPlugin(),
  ],
  optimizeDeps: {
    esbuildOptions: {
      target: "es2020",
    },
  },
  build: {
    target: "es2020",
    sourcemap: true,
    rollupOptions: {},
  },
})
