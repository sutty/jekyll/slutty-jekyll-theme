# sutty-base-jekyll-theme

CAMBIAME: Esta es una plantilla base para comenzar cualquier plantilla
relacionada con Sutty, con el entorno de desarrollo que usamos según
nuestro flujo de trabajo.

```bash
# Desde el directorio de trabajo
cd ~/Sutty
# Instalar sutty-cli antes de empezar
gem install sutty-cli
# Iniciar una plantilla
sutty-cli theme CAMBIAME
# Se creó el repositorio
cd CAMBIAME-jekyll-theme
```

Para empezar, hay que buscar y reemplazar en todos los archivos donde
diga `CAMBIAME`.

También hay que instalar
[sutty.local](https://0xacab.org/sutty/sutty.local/):

```bash
# Desde el directorio de trabajo
cd ~/Sutty
# Clonar el repositorio
git clone https://0xacab.org/sutty/sutty.local.git
cd sutty.local
make all domain domain=sutty.local
cd .. # Volver al directorio de trabajo
```

Esto genera un certificado a nivel local que luego va a usar el
servidor de desarrollo.

## Desarrollo

Siempre trabajamos desde el directorio raíz del repositorio:

```bash
cd ~/Sutty/CAMBIAME-jekyll-theme
```

Primero hay que iniciar los servidores, antes hay que instalar el
paquete `nghttp2`.

```bash
make serve
```

A partir de este momento podemos editar los archivos.  Para probar lo
que estamos haciendo, compilamos el sitio.

```bash
make build
```

Hay que hacer esto cada vez que queremos probar cambios.

**Nota:** No usamos `jekyll serve` porque entra en conflicto con algunos
de nuestros complementos y los sitios grandes tardan en compilarse de
todas formas.

## Actualizar

Podemos traer cambios de la base:

```bash
git pull upstream master
```

Si hubiera conflictos, los podemos resolver con:

```bash
# Esto se hace una sola vez, cambiar vimdiff por la herramienta que
# queramos usar
git config --global mergetool.tool vimdiff
git config --global mergetool.keepBackup false

# Resolver los conflictos
git mergetool
```

## Publicación

Para publicar la plantilla, primero hay que cambiar el nombre al archivo
`sutty-base-jekyll-theme.gemspec`:

```bash
git mv {sutty-base,CAMBIAME}-jekyll-theme.gemspec
```

Cada vez que actualicemos la gema hay que editar
`CAMBIAME-jekyll-theme.gemspec` para cambiar la versión.

Y luego para compilar y publicar la gema:

```bash
gem build CAMBIAME-jekyll-theme.gemspec
gem push CAMBIAME-VERSION.gemspec
```

También hay que actualizar la versión en el
[skel](https://0xacab.org/sutty/skel.sutty.nl/).


## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "CAMBIAME-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: CAMBIAME-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install CAMBIAME-jekyll-theme

## Usage

CAMBIAME

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
https://0xacab.org/sutty/jekyll/CAMBIAME-jekyll-theme. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty's code of
conduct](http://sutty.nl/en/code-of-conduct).

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme,
run `bundle exec jekyll serve` and open your browser at
`http://localhost:4000`. This starts a Jekyll server using your
theme. Add pages, documents, data, etc. like normal to test your theme's
contents. As you make modifications to your theme and to your content,
your site will regenerate and you should see the changes in the browser
after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`,
`_sass` and `assets` tracked with Git will be bundled.

To add a custom directory to your theme-gem, please edit the array in
`CAMBIAME-jekyll-theme.gemspec` accordingly.

## License

The theme is available as open source under the terms of the [Antifacist
MIT License](LICENSE.txt).

