# frozen_string_literal: true

module Jekyll
  module Filters
    module Base64
      def base64(input)
        require 'base64'

        ::Base64.encode64 ::File.open(input, 'rb').read
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Base64)
